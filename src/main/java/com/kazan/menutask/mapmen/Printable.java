package com.kazan.menutask.mapmen;

public interface Printable {

    void print();
}